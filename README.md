# Backports for RHEL

This repo contains the scripts and CI jobs for building newer versions of
different pieces of software for RHEL, to be used by CKI [RHEL builder
container images][containers].

The production artifacts can be downloaded from the [lookaside bucket].

The precise versions and packages backported for the various RHEL versions can
be found in [.gitlab-ci.yml](.gitlab-ci.yml). Dependencies are kept up to date
via [Renovate].

[containers]: https://gitlab.com/cki-project/containers/
[lookaside bucket]: https://s3.amazonaws.com/arr-cki-prod-lookaside/index.html?prefix=lookaside/backports/production
[Renovate]: https://github.com/renovatebot/renovate
